#!/usr/bin/env dub
/+ dub.sdl:
   name "mindfile"
   dependency "mind" version="~master"
 +/
import mind;
import std.stdio : writeln;
import std.format : format;
import std.range : iota;
import std.algorithm : map;
import std.array : array;
import core.thread.osthread : Thread;
import core.time : msecs;

int main(string[] args)
{
    auto all = task("all", null, (t) {});
    for (int i = 0; i < 20; ++i)
    {
        auto fileName = "out/file-%s.txt".format(i);
        all.enhance(fileName);
        description(fileName);
        file(fileName, null, (t) {
            Thread.sleep(100.msecs);
            sh("touch %s".format(t.name));
        },);
    }
    return mindMain(args);
}
