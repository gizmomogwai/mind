module mind;

import androidlogger : AndroidLogger;
import argparse : ansiStylingArgument, CLI, Config, NamedArgument, Optional, PositionalArgument;
import asciitable : AsciiParts, AsciiTable;
import mir.deser.json : deserializeJson;
import mir.serde : serdeIgnoreUnexpectedKeys, serdeKeys, serdeFlexible, serdeProxy;
import profiled : NoopProfiler, Profiler, theProfiler;
import std.algorithm : count, filter, fold, map, sort;
import std.array : array, byPair;
import std.concurrency : receive, send, thisTid, Tid;
import std.datetime.date : DateTime;
import std.datetime.date : DateTime;
import std.datetime.systime : Clock, SysTime;
import std.datetime.timezone : UTC;
import std.file : exists, isFile, timeLastModified;
import std.format : format;
import std.functional : not;
import std.logger : info, LogLevel, sharedLog, trace, warning, error;
import std.parallelism : task, taskPool, TaskPool;
import std.process : executeShell, spawnShell, wait;
import std.range : empty;
import std.stdio : File, stderr, write, writeln;
import std.conv : to;

version (unittest)
{
    import unit_threaded : should;
}
private:
/// All Tasks
static Task[string] ALL;

/// Description for the next task that is defined
static string currentDescription = "";

/// No time
private static SysTime NO_TIME = SysTime(DateTime(0, 1, 1, 0, 0, 0), UTC());

/// Sent to the main loop if a task is done
struct Done
{
    string taskName;
}

/// Sent to the main loop if a task failed
struct Failure
{
    string taskName;
    string message;
}

alias Code = void delegate(Task);

void execute(Tid main, Task t)
{
    try
    {
        const _ = theProfiler.start(t.name);
        t.execute;
        main.send(Done(t.name));
    }
    catch (Exception e)
    {
        main.send(Failure(t.name, e.message.idup));
    }
}

void startWork(Task[string] needed, TaskPool taskPool, Tid mainTid)
{
    foreach (taskName, task; needed)
    {
        if (task.state == State.READY_TO_RUN)
        {
            task.state = State.RUNNING;
            taskPool.enqueue(mainTid, task);
        }
    }
}

void enqueue(TaskPool taskPool, Tid mainTid, Task t)
{
    taskPool.put(task!(execute)(mainTid, t));
}

auto createTaskPool(uint jobs)
{
    switch (jobs)
    {
    case 0:
        return new TaskPool();
    default:
        return new TaskPool(jobs);
    }
}

void update(ref Task[string] needed, string[] taskNames)
{
    foreach (taskName; taskNames)
    {
        auto task = getTask(taskName);
        needed[taskName] = task;
        needed.update(task.prerequisites);
    }
}

void run(string[] neededTasks, uint jobs, bool profile)
{
    Tid mainTid = thisTid;
    auto taskPool = createTaskPool(jobs);
    scope (exit)
    {
        taskPool.stop;
    }

    Task[string] needed;
    needed.update(neededTasks);

    auto tasksCompleted = needed.byValue
        .map!(i => i.calcState)
        .count!(s => s == State.DONE);
    needed.startWork(taskPool, mainTid);

    while (tasksCompleted < needed.length)
    {
        // dfmt off
        receive(
          (Done done) {
              tasksCompleted++;
              auto task = ALL[done.taskName];
              task.state = State.DONE;
              foreach (t; task.dependents)
              {
                  if (t.name in needed) {
                      if (t.state != State.DONE && t.state != State.RUNNING)
                      {
                          t.state = State.UNKNOWN;
                          t.calcState();
                          if (t.state == State.READY_TO_RUN)
                          {
                              t.state = State.RUNNING;
                              taskPool.enqueue(mainTid, t);
                          }
                      }
                  }
              }
          },
          (Failure failure)
          {
              throw new Exception("Problems with %s: %s".format(failure.taskName, failure.message));
          },
        );
        // dfmt on
        info("progress: %s/%s".format(tasksCompleted, needed.length));
    }
    info("progress: all done");
    if (profile)
    {
        theProfiler.dumpJson("profile.json");
    }
}

enum State
{
    /// State not yet calculated
    UNKNOWN,
    /// Cannot be executed, as prerequisites are missing
    BLOCKED,
    /// Can be executed
    READY_TO_RUN,
    /// While executing
    RUNNING,
    /// Done
    DONE,
}

State prerequisiteMissing(State s)
{
    final switch (s)
    {
    case State.UNKNOWN:
        return State.BLOCKED;
    case State.BLOCKED:
        return State.BLOCKED;
    case State.READY_TO_RUN:
        return State.BLOCKED;
    case State.RUNNING:
        throw new Exception("State is already RUNNING but prerequisite is reported missing");
    case State.DONE:
        throw new Exception("State is already DONE but prerequisite is reported missing");
    }
}

State prerequisiteNewer(State s)
{
    final switch (s)
    {
    case State.UNKNOWN:
        return State.READY_TO_RUN;
    case State.BLOCKED:
        return State.BLOCKED;
    case State.READY_TO_RUN:
        return State.READY_TO_RUN;
    case State.RUNNING:
        throw new Exception("State is already RUNNING but prerequisite is reported newer");
    case State.DONE:
        throw new Exception("State is already DONE but prerequisite is reported newer");
    }
}

State selfMissing(State s)
{
    final switch (s)
    {
    case State.UNKNOWN:
        return State.READY_TO_RUN;
    case State.BLOCKED:
        return State.BLOCKED;
    case State.READY_TO_RUN:
        return State.READY_TO_RUN;
    case State.RUNNING:
        throw new Exception("State is already RUNNING but reported missing");
    case State.DONE:
        throw new Exception("State is already DONE but reported missing");
    }
}

private Task getTask(string name)
{
    if (name !in ALL)
    {
        register(new ImplicitFileTask(name));
    }
    return ALL[name];
}

class Task
{
private:
    static void inverseGraph(string taskName, Task dependent = null)
    {
        if (taskName in ALL)
        {
            auto task = ALL[taskName];
            if (dependent !is null)
            {
                task.dependents ~= dependent;
            }
            foreach (prerequisite; task.prerequisites)
            {
                if (prerequisite in ALL)
                {
                    inverseGraph(prerequisite, task);
                }
            }
        }
        else
        {
            throw new Exception("Task '%s' not defined".format(taskName));
        }
    }

    /// Inverses the prerequisite graph for the needed tasks
    static void prepareForExecution(Task dependent = null)
    {
        foreach (name, task; ALL)
        {
            inverseGraph(task.name, dependent);
        }
        foreach (name, task; ALL)
        {
            const _ = task.calcState();
        }
    }

    string description;
    public const string name;
    /// Needed to execute this task
    public string[] prerequisites;
    /// Are dependend on this task
    Task[] dependents;
    Code code;
    State state = State.UNKNOWN;

    bool timestampCalculated = false;
    SysTime calculatedTimestamp;
    this(string name, string[] prerequisites, Code code)
    {
        this.description = currentDescription;
        currentDescription = null;
        this.name = name;
        this.prerequisites = prerequisites;
        this.code = code;
    }

    /// Add a prerequisite to this task
    public auto enhance(string prerequisite)
    {
        prerequisites ~= prerequisite;
        return this;
    }

    /// Add a prerequisite to this task
    public auto enhance(Task prerequisite)
    {
        prerequisites ~= prerequisite.name;
        return this;
    }

    /// Calc and cache the timestamp of this artifact
    protected SysTime timestamp()
    {
        if (!timestampCalculated)
        {
            calculatedTimestamp = calcTimestamp();
            timestampCalculated = true;
        }
        return calculatedTimestamp;
    }

    /// Do just the timestamp calculation (should be overridden by subclasses)
    protected SysTime calcTimestamp()
    {
        return Clock.currTime;
    }

    /++ Calcs and caches the state of a task by looking at the state
     + of the prerequisites.
     + If there are no prerequisites, or all prerequisites are older
     + than this task, the defaultState method is used. Should be
     + overridden by subtasks (e.g. the implicitfiletask is just done
     + in this case but e.g. a netlify deploy could be done or never
     + have been deployed.
     +/
    protected State calcState()
    {
        if (state != State.UNKNOWN)
        {
            return state;
        }
        foreach (p; prerequisites)
        {
            auto prerequisite = getTask(p);
            prerequisite.calcState;
        }

        foreach (p; prerequisites)
        {
            auto prerequisite = getTask(p);
            if (prerequisite.state != State.DONE)
            {
                state = state.prerequisiteMissing;
            }
            else if (prerequisite.isNewer(this))
            {
                state = state.prerequisiteNewer;
            }
        }

        if (state == State.UNKNOWN)
        {
            state = defaultState;
        }

        return state;
    }

    protected State defaultState()
    {
        return State.READY_TO_RUN;
    }

    void execute()
    {
        code(this);
    }

    bool isNewer(Task task)
    {
        return timestamp > task.timestamp;
    }
}

@("isNewer") unittest
{
    (SysTime(DateTime(2000, 1, 1, 0, 0), UTC()) < SysTime(DateTime(2023, 1, 1, 0, 0), UTC()))
        .should == true;
}

/++ FileTasks that has no code attached.
 + This is used for prerequisites that are just filenames.
 +/
class ImplicitFileTask : Task
{
    this(string taskName)
    {
        super(taskName, [], (Task t) {});
    }

    override protected State calcState()
    {
        if (state != State.UNKNOWN)
        {
            return state;
        }
        state = name.exists ? State.DONE : State.BLOCKED;
        return state;
    }

    override protected SysTime calcTimestamp()
    {
        if (name.exists)
        {
            if (name.isFile)
            {
                return name.timeLastModified;
            }
            else
            {
                return NO_TIME;
            }
        }
        return super.calcTimestamp();
    }
}

class FileTask : Task
{
private:
    this(string name, string[] prerequisites, Code code)
    {
        super(name, prerequisites, code);
        this.state = State.UNKNOWN;
    }

    override protected State defaultState()
    {
        if (name.exists)
        {
            return State.DONE;
        }
        return state.selfMissing;
    }

    override protected SysTime calcTimestamp()
    {
        if (name.exists)
        {
            if (name.isFile)
            {
                return name.timeLastModified;
            }
            else
            {
                return NO_TIME;
            }
        }
        return super.calcTimestamp;
    }
}

string getShellOutput(string command)
{
    auto result = command.executeShell;
    if (result.status != 0)
    {
        result.output.to!string.error;
        throw new Exception("Cannot execute '%s'".format(command));
    }
    return result.output.to!string;
}

class PodmanImageTask : Task
{
private:
    this(string name, string[] prerequisites, string prefix)
    {
        super(name, prerequisites, (t) {
            "%s && podman build . --tag %s".format(prefix, t.name).getShellOutput.info;
        });
        this.state = State.UNKNOWN;
    }

    override protected SysTime calcTimestamp()
    {
        auto result = "podman inspect %s".format(name).executeShell;
        if (result.status != 0)
        {
            return NO_TIME;
        }
        return parsePodmanInspectOutput(result.output)[0].created.systime;
    }

    protected override State defaultState()
    {
        if (timestamp == NO_TIME)
        {
            return State.READY_TO_RUN;
        }
        return State.DONE;
    }
}

class NetlifyDeploy : Task
{
    /// As in netlify sites:list
    string siteId;
    this(string name, string siteId, string imageTag, string netlifyAuth, string[] prerequisites)
    {
        super(name ~ siteId, prerequisites, (t) {
            sh("podman run --rm --tty -v$(pwd):/ws --entrypoint=/usr/bin/netlify --workdir=/ws %s deploy --prod --auth=%s --site=%s --dir=output/encrypted"
                .format(imageTag, netlifyAuth, name));
        });
        this.siteId = siteId;
    }

    override protected SysTime calcTimestamp()
    {
        try
        {
            return `netlify api getSite --data '{"site_id":"%s"}'`.format(siteId)
                .getShellOutput.parseNetlifyStatus.updatedAt.systime;
        }
        catch (Exception e)
        {
            return NO_TIME;
        }
    }

    override protected State defaultState()
    {
        if (timestamp == NO_TIME)
        {
            return State.READY_TO_RUN;
        }
        return State.DONE;
    }
}

/// Set the description of the next task that is defined
public void description(string d)
{
    currentDescription = d;
}

auto register(T)(T t)
{
    if (t.name in ALL)
    {
        throw new Exception("Task '%s' is already registered".format(t.name));
    }
    ALL[t.name] = t;
    return t;
}

/// Define a new task
public Task task(string name, string[] prerequisites = null, Code code = (t) {})
{
    return new Task(name, prerequisites, code).register;
}

/// Define a new file task
public Task file(string name, string[] prerequisites = null, Code code = (t) {})
{
    return new FileTask(name, prerequisites, code).register;
}

/// Define a podman image task
public Task podmanImage(string name, string[] prerequisites = null, string prefix = "")
{
    return new PodmanImageTask(name, prerequisites, prefix).register;
}

/// Deploy to netlify site
public Task deployToNetlify(string name, string siteId, string imageTag,
        string netlifyAuth, string[] prerequisites)
{
    return new NetlifyDeploy(name, siteId, imageTag, netlifyAuth, prerequisites).register;
}

/++
 + Run command either with connected stdout, stderr streams (not
 + buffered) or with just one output at the end of the execution
 + (buffered). The 2nd option might be better when you have very
 + parallel builds.
 +/
public void sh(string command, bool buffered = false)
{
    trace("sh: Executing ", command);
    if (buffered)
    {
        auto result = command.executeShell;
        trace("sh:  => ", result.status, "\nOutput:\n", result.output);
        if (result.status != 0)
        {
            throw new Exception("%s failed with %s\n%s".format(command,
                    result.status, result.output));
        }
    }
    else
    {
        auto pid = command.spawnShell;
        const status = wait(pid);
        if (status != 0)
        {
            throw new Exception("%s failed with %s".format(command, status));
        }
    }
}

/// Write a timestamp into a file
public void writeTimestamp(string file)
{
    File(file, "w").write(Clock.currTime.toISOExtString);
}

int mindMain_(Arguments args)
{
    shared logger = cast(shared) new AndroidLogger(stderr,
            args.withColors == Config.StylingMode.on, LogLevel.all, args.developerMode,);
    sharedLog = logger;

    "# Running".info;
    theProfiler = args.profile ? new Profiler() : new NoopProfiler();
    {
        const _ = theProfiler.start("preparation");
        Task.prepareForExecution();
    }

    if (args.listTasks)
    {
        // dfmt off
        auto tasks = args.all ?
            ALL.byPair.array :
            ALL.byPair.filter!(not!(kv => kv.value.description.empty)).array;
        auto parts = new AsciiParts;
        parts.vertical = " # ";
        tasks
            .sort!("a.key < b.key")
            .fold!(
                (table, kv) => table
                .row.add(kv.key ~ "  ", "%s".format(kv.value.state), kv.value.description ? kv.value.description : " ").table)
                (new AsciiTable(3).header.add("# Name", "Run", "Description").table)
            .format
                .parts(parts)
                .columnSeparator(true)
            .writeln;
        // dfmt on
        return 0;
    }

    run(args.tasks, args.jobs, args.profile);

    return 0;
}

struct Arguments
{
    @NamedArgument("tasks", "T")
    bool listTasks = false;

    @NamedArgument("all", "A")
    bool all = false;

    @NamedArgument("jobs", "j")
    uint jobs = 0;

    @(PositionalArgument(0).Optional())
    string[] tasks;

    @NamedArgument bool profile = false;

    @NamedArgument static auto withColors = ansiStylingArgument;

    @NamedArgument bool developerMode = false;
}

/// main to call from mindfiles
public int mindMain(string[] args)
{
    return CLI!(Arguments).parseArgs!(mindMain_)(args[1 .. $]);
}

@serdeIgnoreUnexpectedKeys struct PodmanImage
{
    @serdeKeys("Created")
    @serdeProxy!string SysTimeProxy created;
}

auto workaround(string s)
{
    return SysTime.fromISOExtString(s);
}

struct SysTimeProxy
{
    SysTime systime;
    this(string s) pure @trusted
    {
        auto h = cast(SysTime function(string) pure)(&workaround);
        systime = h(s);
    }
}

enum podmanImageInspectOutput = `[
     {
          "Id": "61e1d74f6741559121695519b03d55557878c49580ba48579e97a2400783aeef",
          "Digest": "sha256:9a91f6a3aedcf1be9448c634c43ccd37968f2496b19d546026511ab2cd331987",
          "RepoTags": [
               "localhost/borealiscamping:latest",
               "localhost/borealiscomping:latest",
               "localhost/borealis:latest",
               "localhost/alpengluehen:latest"
          ],
          "RepoDigests": [
               "localhost/alpengluehen@sha256:9a91f6a3aedcf1be9448c634c43ccd37968f2496b19d546026511ab2cd331987",
               "localhost/borealis@sha256:9a91f6a3aedcf1be9448c634c43ccd37968f2496b19d546026511ab2cd331987",
               "localhost/borealiscamping@sha256:9a91f6a3aedcf1be9448c634c43ccd37968f2496b19d546026511ab2cd331987",
               "localhost/borealiscomping@sha256:9a91f6a3aedcf1be9448c634c43ccd37968f2496b19d546026511ab2cd331987"
          ],
          "Parent": "d09e2a81f77b59c7f758f47755fb9fd980eead66140fa42a506981fd5a5a4520",
          "Comment": "",
          "Created": "2023-03-21T19:00:37.424282114Z",
          "Config": {
               "Env": [
                    "PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin",
                    "HOME=/root"
               ],
               "Entrypoint": [
                    "emacs.sh"
               ],
               "Labels": {
                    "io.buildah.version": "1.29.0",
                    "org.opencontainers.image.ref.name": "ubuntu",
                    "org.opencontainers.image.version": "22.04"
               }
          },
          "Version": "",
          "Author": "",
          "Architecture": "amd64",
          "Os": "linux",
          "Size": 879695305,
          "VirtualSize": 879695305,
          "GraphDriver": {
               "Name": "overlay",
               "Data": {
                    "LowerDir": "/var/home/core/.local/share/containers/storage/overlay/871dfcafe828a70db24bc97d10ca9eb79aa599a45d2b5082606f64fab2ef7a10/diff:/var/home/core/.local/share/containers/storage/overlay/e1c573a1ff851430b2d540a4cb597b7513f9330a050899547e5ed1ffd99dfe4c/diff:/var/home/core/.local/share/containers/storage/overlay/ff95055d6ce44545f82cc9a8d0b426e24cb146c921f3dc01422ed2c4d5f31b03/diff:/var/home/core/.local/share/containers/storage/overlay/b635bcf27497d338714eb4890edd55302833acd210d51626e9cb6347b8f3e708/diff:/var/home/core/.local/share/containers/storage/overlay/6651c72dc4491fcec5feecc60dbc862c1a81f8e8b69d37873faa57a677a5d3cc/diff:/var/home/core/.local/share/containers/storage/overlay/6bf2b30201f947116d38aac56a51f1a2694f1bc14b84085af7982cf73522a087/diff:/var/home/core/.local/share/containers/storage/overlay/d981e33950dffd8f03e69a9cfc61a1fb0e905212d1a75c63a78dd5da1a6dcedb/diff:/var/home/core/.local/share/containers/storage/overlay/80c60ac72ef1d1cdc7b6d1c5ad7e54c66009dc654171e9df3bd652abf2e013dd/diff:/var/home/core/.local/share/containers/storage/overlay/97f36f0506e3b78ba6380c3dfbfc79727beb0d83a85e2f948e64d3bfcc69bced/diff:/var/home/core/.local/share/containers/storage/overlay/d722ce046b84467c2d7ee03ac04788100adb5b9c63ca61c452e9a92496113c83/diff:/var/home/core/.local/share/containers/storage/overlay/c5ff2d88f67954bdcf1cfdd46fe3d683858d69c2cadd6660812edfc83726c654/diff",
                    "UpperDir": "/var/home/core/.local/share/containers/storage/overlay/b75f3ea2352201c6fe186d06df037aaedb5ad582491052e21fea38a0585fa5a3/diff",
                    "WorkDir": "/var/home/core/.local/share/containers/storage/overlay/b75f3ea2352201c6fe186d06df037aaedb5ad582491052e21fea38a0585fa5a3/work"
               }
          },
          "RootFS": {
               "Type": "layers",
               "Layers": [
                    "sha256:c5ff2d88f67954bdcf1cfdd46fe3d683858d69c2cadd6660812edfc83726c654",
                    "sha256:548f90192be9d3d276dca6c2d48956efedd2349aaa32815c2b616496e9e879f5",
                    "sha256:a27be1708146ff3a3d2047644a02e8a43116965e0ccb6bad3a5889391ed30039",
                    "sha256:a1e3bb0a991b82ad3bf722cfd704e4227cce79a8a73ec98468300989df745e88",
                    "sha256:56ff3929813dc31d05344a2d520ac6320c6be8fb125b8c23b31d16d6830da531",
                    "sha256:8d7ba18bf23950c0bc9f6ec8ae5ee3bb17cdc26791d9c84761f5954a065a56fd",
                    "sha256:8daa00826d632b6f343e97266ebe31eb22daf35e10b31db1e9954131b7a57297",
                    "sha256:7e974ebe47180aca278110d45f76a479b65a21b05f83de79ee9e87989bcf1ab9",
                    "sha256:adf4051f2470dc3c569e4711fc7392cce4027f97f6029dd9d287ce79b0d4b2d0",
                    "sha256:5cfe01c6eda1567432325dcfe01495c003d8b6b8e5788fe70172be13717b1cd9",
                    "sha256:f046636d823abea8d5da1d25e96360332174b23ccdaec0bbdf615d3e204a824d",
                    "sha256:18b221273a5d35ba9cbe03b57d2f81d3110ea2adb3b2e6db7fa5a4a7089f4947"
               ]
          },
          "Labels": {
               "io.buildah.version": "1.29.0",
               "org.opencontainers.image.ref.name": "ubuntu",
               "org.opencontainers.image.version": "22.04"
          },
          "Annotations": {
               "org.opencontainers.image.base.digest": "sha256:9bf7ffa4fb8cec641c0541b09d6bde21a3c73fe564d73489ba227429b8d034bc",
               "org.opencontainers.image.base.name": "docker.io/library/ubuntu:22.04"
          },
          "ManifestType": "application/vnd.oci.image.manifest.v1+json",
          "User": "",
          "History": [
               {
                    "created": "2023-01-26T04:57:59.893830181Z",
                    "created_by": "/bin/sh -c #(nop)  ARG RELEASE",
                    "empty_layer": true
               },
               {
                    "created": "2023-01-26T04:57:59.959936538Z",
                    "created_by": "/bin/sh -c #(nop)  ARG LAUNCHPAD_BUILD_ARCH",
                    "empty_layer": true
               },
               {
                    "created": "2023-01-26T04:58:00.031088326Z",
                    "created_by": "/bin/sh -c #(nop)  LABEL org.opencontainers.image.ref.name=ubuntu",
                    "empty_layer": true
               },
               {
                    "created": "2023-01-26T04:58:00.098806406Z",
                    "created_by": "/bin/sh -c #(nop)  LABEL org.opencontainers.image.version=22.04",
                    "empty_layer": true
               },
               {
                    "created": "2023-01-26T04:58:02.097080644Z",
                    "created_by": "/bin/sh -c #(nop) ADD file:18e71f049606f6339ce7a995839623f50e6ec6474bfd0a3a7ca799db726f47f6 in / "
               },
               {
                    "created": "2023-01-26T04:58:02.385638665Z",
                    "created_by": "/bin/sh -c #(nop)  CMD [\"/bin/bash\"]",
                    "empty_layer": true
               },
               {
                    "created": "2023-03-21T18:54:50.282098394Z",
                    "created_by": "/bin/sh -c apt update",
                    "comment": "FROM docker.io/library/ubuntu:22.04"
               },
               {
                    "created": "2023-03-21T18:55:26.917421627Z",
                    "created_by": "/bin/sh -c apt install --yes curl sudo",
                    "comment": "FROM 18c8f9a6e599"
               },
               {
                    "created": "2023-03-21T18:56:17.690562708Z",
                    "created_by": "/bin/sh -c curl -sL https://deb.nodesource.com/setup_16.x | bash -",
                    "comment": "FROM ac049f938576"
               },
               {
                    "created": "2023-03-21T18:56:39.960193682Z",
                    "created_by": "/bin/sh -c apt install --yes nodejs",
                    "comment": "FROM e7f1c6ff2044"
               },
               {
                    "created": "2023-03-21T18:58:51.602823265Z",
                    "created_by": "/bin/sh -c npm install --global staticrypt netlify-cli",
                    "comment": "FROM e9ba439a9ddc"
               },
               {
                    "created": "2023-03-21T19:00:31.549317493Z",
                    "created_by": "/bin/sh -c apt install --yes emacs-nox git graphviz",
                    "comment": "FROM 0b97d00f1120"
               },
               {
                    "created": "2023-03-21T19:00:33.201326871Z",
                    "created_by": "/bin/sh -c mkdir --parents /root/.emacs.d",
                    "comment": "FROM 2f7b40103062"
               },
               {
                    "created": "2023-03-21T19:00:33.244011785Z",
                    "created_by": "/bin/sh -c #(nop) ENV HOME=/root",
                    "comment": "FROM 22b1cba98cdb",
                    "empty_layer": true
               },
               {
                    "created": "2023-03-21T19:00:33.391663234Z",
                    "created_by": "/bin/sh -c #(nop) COPY file:4f66e1c74cc1d2f75c3c30c6090e13786795ff1d36fe6f4d92ac0904e5e491c5 in /root/.emacs.d/init.el ",
                    "comment": "FROM fd055a7b22df"
               },
               {
                    "created": "2023-03-21T19:00:34.530702213Z",
                    "created_by": "/bin/sh -c emacs --batch --no-init-file --no-splash --no-window-system --load /root/.emacs.d/init.el --execute '(kill-emacs)' || true",
                    "comment": "FROM 4d4f33195829"
               },
               {
                    "created": "2023-03-21T19:00:34.70586501Z",
                    "created_by": "/bin/sh -c #(nop) COPY file:d2bb1d611449e4c045998f5b1803fe5fa043361fc7137938d943368923685720 in /usr/bin ",
                    "comment": "FROM cd9d8d4d5f56"
               },
               {
                    "created": "2023-03-21T19:00:36.61949911Z",
                    "created_by": "/bin/sh -c chmod --recursive a+rwx /root",
                    "comment": "FROM 3651db52a0c0"
               },
               {
                    "created": "2023-03-21T19:00:37.424548284Z",
                    "created_by": "/bin/sh -c #(nop) ENTRYPOINT [\"emacs.sh\"]",
                    "comment": "FROM d09e2a81f77b",
                    "empty_layer": true
               }
          ],
          "NamesHistory": [
               "localhost/alpengluehen:latest",
               "localhost/borealis:latest",
               "localhost/borealiscomping:latest",
               "localhost/borealiscamping:latest"
          ]
     }
]`;

auto parsePodmanInspectOutput(string s)
{
    return deserializeJson!(PodmanImage[])(s);
}

@("parse podman inspect output")
unittest
{
    parsePodmanInspectOutput(podmanImageInspectOutput)[0].created.systime.should
        == SysTimeProxy("2023-03-21T19:00:37.424282114Z").systime;
}

@serdeIgnoreUnexpectedKeys struct NetlifyStatus
{
    @serdeKeys("updated_at")
    @serdeProxy!string SysTimeProxy updatedAt;
}

enum netlifyStatusOutput = `{
  "id": "26cd0bd7-14bf-44f2-8fe9-fca82f98a1bd",
  "site_id": "26cd0bd7-14bf-44f2-8fe9-fca82f98a1bd",
  "plan": "nf_team_dev",
  "ssl_plan": null,
  "premium": false,
  "claimed": true,
  "name": "borealiscamping",
  "custom_domain": null,
  "branch_deploy_custom_domain": null,
  "deploy_preview_custom_domain": null,
  "domain_aliases": [],
  "password": null,
  "password_hash": null,
  "sso_login": false,
  "sso_login_context": "all",
  "notification_email": null,
  "url": "http://borealiscamping.netlify.app",
  "admin_url": "https://app.netlify.com/sites/borealiscamping",
  "deploy_id": "64203e973b34862e1af84370",
  "build_id": "",
  "deploy_url": "http://64203e973b34862e1af84370--borealiscamping.netlify.app",
  "state": "current",
  "screenshot_url": "https://d33wubrfki0l68.cloudfront.net/64203e973b34862e1af84370/screenshot_2023-03-26-12-46-19-0000.png",
  "created_at": "2023-03-22T19:12:47.211Z",
  "updated_at": "2023-03-26T12:46:23.566Z",
  "user_id": "5a7a3a06df99536f1097eabb",
  "error_message": null,
  "ssl": false,
  "ssl_url": "https://borealiscamping.netlify.app",
  "force_ssl": null,
  "ssl_status": null,
  "max_domain_aliases": 100,
  "build_settings": {},
  "processing_settings": {
    "css": {
      "bundle": true,
      "minify": true
    },
    "js": {
      "bundle": true,
      "minify": true
    },
    "images": {
      "optimize": true
    },
    "html": {
      "pretty_urls": true
    },
    "skip": true,
    "ignore_html_forms": false
  },
  "prerender": null,
  "prerender_headers": null,
  "deploy_hook": null,
  "published_deploy": {
    "id": "64203e973b34862e1af84370",
    "site_id": "26cd0bd7-14bf-44f2-8fe9-fca82f98a1bd",
    "build_id": null,
    "state": "ready",
    "name": "borealiscamping",
    "url": "http://borealiscamping.netlify.app",
    "ssl_url": "https://borealiscamping.netlify.app",
    "admin_url": "https://app.netlify.com/sites/borealiscamping",
    "deploy_url": "http://64203e973b34862e1af84370--borealiscamping.netlify.app",
    "deploy_ssl_url": "https://64203e973b34862e1af84370--borealiscamping.netlify.app",
    "created_at": "2023-03-26T12:46:15.968Z",
    "updated_at": "2023-03-26T12:46:23.556Z",
    "user_id": "5a7a3a06df99536f1097eabb",
    "error_message": null,
    "required": [],
    "required_functions": [],
    "commit_ref": null,
    "review_id": null,
    "branch": null,
    "commit_url": null,
    "skipped": null,
    "locked": null,
    "log_access_attributes": {
      "type": "firebase",
      "url": "https://netlify-builds7.firebaseio.com/deploys/64203e973b34862e1af84370/log",
      "database": "netlify-builds7",
      "endpoint": "https://netlify-builds7.firebaseio.com",
      "path": "/deploys/64203e973b34862e1af84370/log",
      "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ2IjowLCJpYXQiOjE2Nzk5Mzg0NzMsImQiOnsidWlkIjoiIn19.Q6jaCUI--fsaui8dqNcDJbtgvu9mhuWYZ9IRqoj9od4"
    },
    "title": null,
    "review_url": null,
    "published_at": "2023-03-26T12:46:19.754Z",
    "context": "production",
    "deploy_time": 3,
    "available_functions": [],
    "screenshot_url": "https://d33wubrfki0l68.cloudfront.net/64203e973b34862e1af84370/screenshot_2023-03-26-12-46-19-0000.png",
    "site_capabilities": {
      "title": "Netlify Team Free",
      "asset_acceleration": true,
      "form_processing": true,
      "cdn_propagation": "partial",
      "domain_aliases": true,
      "secure_site": false,
      "sso_secure_site": false,
      "secure_site_context": false,
      "prerendering": true,
      "proxying": true,
      "ssl": "custom",
      "rate_cents": 0,
      "yearly_rate_cents": 0,
      "ipv6_domain": "cdn.makerloop.com",
      "branch_deploy": true,
      "managed_dns": true,
      "geo_ip": true,
      "split_testing": true,
      "id": "nf_team_dev",
      "cdn_tier": "reg"
    },
    "committer": null,
    "skipped_log": null,
    "manual_deploy": false,
    "file_tracking_optimization": true,
    "plugin_state": "none",
    "lighthouse_plugin_scores": null,
    "links": {
      "permalink": "https://64203e973b34862e1af84370--borealiscamping.netlify.app",
      "alias": "https://borealiscamping.netlify.app",
      "branch": null
    },
    "framework": null,
    "entry_path": null,
    "views_count": null,
    "function_schedules": [],
    "public_repo": null,
    "pending_review_reason": null,
    "lighthouse": null
  },
  "managed_dns": true,
  "jwt_secret": null,
  "jwt_roles_path": "app_metadata.authorization.roles",
  "account_slug": "gizmomogwai",
  "account_name": "Christian Köstlin's team",
  "account_type": "Starter",
  "capabilities": {
    "title": "Netlify Team Free",
    "asset_acceleration": true,
    "form_processing": true,
    "cdn_propagation": "partial",
    "domain_aliases": true,
    "secure_site": false,
    "sso_secure_site": false,
    "secure_site_context": false,
    "prerendering": true,
    "proxying": true,
    "ssl": "custom",
    "rate_cents": 0,
    "yearly_rate_cents": 0,
    "ipv6_domain": "cdn.makerloop.com",
    "branch_deploy": true,
    "managed_dns": true,
    "geo_ip": true,
    "split_testing": true,
    "id": "nf_team_dev",
    "cdn_tier": "reg"
  },
  "dns_zone_id": null,
  "identity_instance_id": null,
  "use_functions": null,
  "use_edge_handlers": null,
  "parent_user_id": null,
  "automatic_tls_provisioning": null,
  "disabled": null,
  "lifecycle_state": "active",
  "id_domain": "26cd0bd7-14bf-44f2-8fe9-fca82f98a1bd.netlify.app",
  "use_lm": null,
  "build_image": "focal",
  "automatic_tls_provisioning_expired": false,
  "analytics_instance_id": null,
  "functions_region": null,
  "functions_config": {
    "site_created_at": "2023-03-22T19:12:47.211Z"
  },
  "plugins": [],
  "account_subdomain": null,
  "functions_env": {},
  "cdp_enabled_contexts": [
    "deploy-preview"
  ],
  "authlify_token_id": null,
  "build_timelimit": null,
  "use_custom_domain_in_all_contexts": false,
  "uses_new_env_var": true,
  "password_context": "all",
  "deploy_retention_in_days": 90,
  "use_envelope": true,
  "default_domain": "borealiscamping.netlify.app"
}`;

NetlifyStatus parseNetlifyStatus(string s)
{
    return deserializeJson!(NetlifyStatus)(s);
}

@("parse netlify status output")
unittest
{
    parseNetlifyStatus(netlifyStatusOutput).updatedAt.systime.should == SysTimeProxy(
            "2023-03-26T12:46:23.566Z").systime;
}
